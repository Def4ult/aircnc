const express = require ('express');
const mongoose = require ('mongoose');
const cors = require ('cors');
const path = require('path');

const socketio = require('socket.io');
const http = require('http');


const routes = require ('./routes');

const app = express();
const server = http.Server(app);
const io = socketio(server);



mongoose.connect('mongodb+srv://AirCNC:ZMONlpN3lhBAIGY7@aircnc-k4til.gcp.mongodb.net/test?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});

const connectedUsers = {};

io.on('connection', socket => {
    const { user_id } = socket.handshake.query;

    connectedUsers[user_id] = socket.id;
});

app.use((req, res, next) => {
    req.io = io;
    req.connectedUsers = connectedUsers;

    return next();
})
// req.query = Acessar query params (para filtros) ex: /teste?idade=20
// req.params = Acessar route params (para edição, delete) ex: /teste/1
// req.body = acessar corpo da requisição (para criação, edição)

app.use('/files', express.static(path.resolve(__dirname, '..', 'uploads')));
app.use(cors());
app.use(express.json());
app.use(routes);

server.listen(3333);